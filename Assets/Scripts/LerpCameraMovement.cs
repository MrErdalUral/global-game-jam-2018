﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpCameraMovement : MonoBehaviour
{

    public Transform Target;


    // Update is called once per frame
    void Update()
    {
        if (Target == null) return;
        transform.position = Vector3.Lerp(transform.position + new Vector3(0, 0, -10), Target.position, Time.deltaTime*5);
    }
}

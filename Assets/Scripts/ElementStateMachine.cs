﻿using System;

public static class ElementStateMachine
{

    public static Element TouchFireOrb(Element element)
    {
        switch (element)
        {
            case Element.Water:
                break;
            case Element.Ice:
                return Element.Water;
            case Element.Fire:
                break;
            case Element.Metal:
                break;
            case Element.Wood:
                break;
            default:
                throw new ArgumentOutOfRangeException("element", element, null);
        }

        return element;
    }

    public static Element TouchIceOrb(Element element)
    {
        switch (element)
        {
            case Element.Water:
                return Element.Ice;
            case Element.Ice:
                break;
            case Element.Fire:
                break;
            case Element.Metal:
                break;
            case Element.Wood:
                break;
            default:
                throw new ArgumentOutOfRangeException("element", element, null);
        }

        return element;
    }
}

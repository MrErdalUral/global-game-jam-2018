﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TwoDimensionalMovement : MonoBehaviour
{

    public float TargetSpeed = 40f;
    public float Acceleration = 1;
    public float JumpAcceleration = 10f;

    [SerializeField]
    private Rigidbody2D _rigidbody2D;

    private PlayerController _playerController;

    void Awake()
    {
        if (!_rigidbody2D)
            _rigidbody2D = GetComponent<Rigidbody2D>();

        _playerController = GetComponent<PlayerController>();
    }

    public void AccelaretedHorizontalMovement(float x)
    {
        var targetVelocity = x * TargetSpeed;
        var timedBasedAcceleration = Time.deltaTime * Acceleration;
        if (!_playerController.IsOnGround)
            timedBasedAcceleration *= .5f;
        if (timedBasedAcceleration > 1) timedBasedAcceleration = 1;
        var velocity = timedBasedAcceleration * targetVelocity + (1 - timedBasedAcceleration) * _rigidbody2D.velocity.x;
        _rigidbody2D.velocity = new Vector2(velocity, _rigidbody2D.velocity.y);
    }
    public void AccelaretedVerticalMovement(float y)
    {
        if (Mathf.Pow(y, 2) <= float.Epsilon) return;
        var targetVelocity = y * TargetSpeed;
        var velocity = targetVelocity;
        _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, velocity);

    }
    public void Jump(float t)
    {
        var force = JumpAcceleration * _rigidbody2D.mass * t;
        _rigidbody2D.AddForce(transform.up * force, ForceMode2D.Impulse);
    }
}

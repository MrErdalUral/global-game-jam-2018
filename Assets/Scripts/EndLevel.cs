﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour
{
    public bool RequireFireOrb;
    public bool RequireIceOrb;

    public string NextLevel;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (NextLevel == null || collision.gameObject.tag != "Player") return;
        var throwScript = collision.GetComponent<OrbThrower>();
        if(RequireFireOrb && !throwScript.IsFireOrbReady)
        {
            //Todo Display message "You need to carry the fire orb If you must ascend into the next level"
            TextController.Instance.SetText("You need to have the exothermic orb.", 3);
            return;
        }
        if(RequireIceOrb && !throwScript.IsIceOrbReady)
        {
            //Todo Display message "You need to carry the ice orb If you must ascend into the next level"
            TextController.Instance.SetText("You need to have the endothermic orb.", 3);
            return;
        }
        if(RequireFireOrb && RequireIceOrb && (!throwScript.IsFireOrbReady || !throwScript.IsIceOrbReady))
        {
            //Todo Display message "You need to carry both orbs If you must ascend into the next level"
            TextController.Instance.SetText("You need to have both orbs.", 3);
            return;
        }
        throwScript.GetComponentInChildren<Animator>().SetTrigger("EndLevel");
        StartCoroutine(LevelEnd());
    }

    private IEnumerator LevelEnd()
    {
        var animator = GetComponent<Animator>();
        animator.SetTrigger("Open Door");
        var t = animator.GetCurrentAnimatorStateInfo(0).length * 2 ;
        yield return new WaitForSeconds(t);
        SceneManager.LoadScene(NextLevel);
    }
}

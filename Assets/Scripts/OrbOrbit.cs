using UnityEngine;

public class OrbOrbit : MonoBehaviour
{
    private GameObject _fireOrb;
    private GameObject _iceOrb;

    public GameObject FireOrbPrefab;
    public GameObject IceOrbPrefab;

    public float OrbitRate;

    private SpriteRenderer _fireSpriteRenderer;
    private SpriteRenderer _iceSpriteRenderer;

    private Color _colorFire;
    private Color _colorIce;

    private OrbThrower _orbThrower;

    private void Awake()
    {
        _orbThrower = GetComponentInParent<OrbThrower>();

        _fireOrb = Instantiate(FireOrbPrefab, transform.position, Quaternion.identity, transform);
        _iceOrb = Instantiate(IceOrbPrefab, transform.position, Quaternion.identity, transform);

        _fireOrb.transform.localScale = new Vector3(1, .55f, 1) * .25f;
        _iceOrb.transform.localScale = new Vector3(1, .55f, 1) * .25f;

        _fireOrb.GetComponent<Collider2D>().enabled = false;
        _iceOrb.GetComponent<Collider2D>().enabled = false;

        _fireOrb.transform.Translate(3, 1, 0);
        _iceOrb.transform.Translate(-2, 1, 0);

        _fireSpriteRenderer = _fireOrb.GetComponent<SpriteRenderer>();
        _iceSpriteRenderer = _iceOrb.GetComponent<SpriteRenderer>();

        _colorFire = _fireSpriteRenderer.color;
        _colorIce = _iceSpriteRenderer.color;
        GetComponentsInChildren<ParticleSystem>()[0].Stop();
        GetComponentsInChildren<ParticleSystem>()[1].Stop();

    }

    private void Update()
    {
        DecideOrbVisibility(_fireSpriteRenderer, _orbThrower.IsFireOrbReady, _colorFire);
        DecideOrbVisibility(_iceSpriteRenderer, _orbThrower.IsIceOrbReady, _colorIce);

        var sin1 = Mathf.Sin(OrbitRate * Time.time) * .05f;
        var sin2 = Mathf.Sin(OrbitRate * Time.time + 1) * .05f;

        _fireOrb.transform.Translate(0, sin1, 0);
        _iceOrb.transform.Translate(0, sin2, 0);
    }

    private void DecideOrbVisibility(SpriteRenderer spriteRenderer, bool isOrbReady, Color color)
    {
        if (isOrbReady && spriteRenderer.color.a < 0.99f)
        {
            spriteRenderer.color = color.WithAlpha(1);

        }
        else if (!isOrbReady && spriteRenderer.color.a > 0.01f)
        {
            spriteRenderer.color = color.WithAlpha(0);
        }
    }
}

using UnityEngine;

public class AudioClipStore : MonoBehaviour
{
    public AudioClip[] FreezeClips;
    public AudioClip[] ThrowBallClips;
    public AudioClip[] FallClips;
    public AudioClip[] DrownClips;
    public AudioClip[] AmbientWindClips;
    public AudioClip[] AmbientFireClips;

    public static AudioClipStore Instance;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
}

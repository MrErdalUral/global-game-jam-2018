﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Extensions
{
    public static bool Contains(this LayerMask layerMask, int other)
    {
        return layerMask == (layerMask | 1 << other);
    }

    public static Color WithAlpha(this Color color, float alpha)
    {
        return new Color(color.r, color.g, color.b, alpha);
    }

    public static float Random(this Vector2 range)
    {
        return UnityEngine.Random.value * (range.y - range.x) + range.x;
    }

    public static T RandomElement<T>(this T[] array)
    {
        return array[Mathf.FloorToInt(UnityEngine.Random.value * array.Length)];
    }

    public static ulong Delay(this AudioClip clip, float seconds)
    {
        return Convert.ToUInt64(Mathf.Ceil(clip.samples * seconds));
    }

    public static AudioSource RandomizePitch(this AudioSource source, float maxOffset)
    {
        source.pitch = 1 + UnityEngine.Random.Range(0, maxOffset);
        return source;
    }

    public static int Level(this Scene scene)
    {
        if (scene.name.StartsWith("Level"))
        {
            return Convert.ToInt32(scene.name[5].ToString());
        }
        return -1;
    }
}


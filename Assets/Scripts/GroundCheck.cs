﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    private PlayerController _playerController;

    // Use this for initialization
    void Awake()
    {
        _playerController = GetComponentInParent<PlayerController>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (_playerController.GroundMask.Contains(collision.gameObject.layer))
        {
            _playerController.IsOnGround = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (_playerController.GroundMask.Contains(collision.gameObject.layer))
        {
            _playerController.IsOnGround = false;
        }
    }
}

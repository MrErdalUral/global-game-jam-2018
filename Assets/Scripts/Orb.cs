﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Orb : MonoBehaviour
{
    public bool IsStuck;

    private Rigidbody2D _rigidBody;
    private float _originalGravity;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Metal")
            IsStuck = true;
    }
    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Metal")
            IsStuck = false;

    }
    void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _originalGravity = _rigidBody.gravityScale;

    }
    void FixedUpdate()
    {
        if (IsStuck)
        {
            _rigidBody.velocity = Vector2.zero;
            _rigidBody.gravityScale = 0;
        }
        else
        {
            _rigidBody.gravityScale = _originalGravity;
        }
    }
}

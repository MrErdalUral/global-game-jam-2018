using UnityEngine;
using UnityEngine.SceneManagement;

public class AmbientNoise : MonoBehaviour
{
    public static AmbientNoise Instance;
    public AudioClip Wind;
    public AudioClip Fire;

    private AudioSource _audioSource;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        _audioSource = AudioManager.Instance.Play(Wind, loop: true);
        _audioSource.spatialBlend = 0;

        StartCoroutine(AudioManager.Instance.RandomizeLoopPitchCoroutine(_audioSource, .2f));

        SceneManager.activeSceneChanged += delegate(Scene arg0, Scene currentScene)
        {
            var level = currentScene.Level();

            if (level > 3 && _audioSource.clip != Fire)
            {
                _audioSource.clip = Fire;
            }
            else if (level < 4 && _audioSource.clip != Wind)
            {
                _audioSource.clip = Wind;
            }

            if (!_audioSource.isPlaying)
            {
                _audioSource = AudioManager.Instance.Play(_audioSource.clip, loop: true);
                _audioSource.spatialBlend = 0;

                StartCoroutine(AudioManager.Instance.RandomizeLoopPitchCoroutine(_audioSource, .2f));
            }
        };
    }
}

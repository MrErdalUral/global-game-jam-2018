﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxController : MonoBehaviour {


    public Transform Parent;
    public float Speed;
    private Vector2 previousPosition;
    private Vector2 currentPosition;
    private MeshRenderer _renderer;

	// Use this for initialization
	void Start () {
        Parent = transform.parent;
        _renderer = GetComponent<MeshRenderer>();

    }
	
	// Update is called once per frame
	void Update () {
        currentPosition = Parent.position;
        var displacement = currentPosition - previousPosition;
        displacement.y = 0;
        _renderer.material.mainTextureOffset += displacement/100 * Speed;
        previousPosition = currentPosition;
	}
}

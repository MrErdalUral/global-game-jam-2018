using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public float MaxHealth;
    public float Health;
    public float FallDamageHeight;

    public Slider Slider;

    private Rigidbody2D _body;
    private PlayerController _player;
    private bool _isOnGroundPrev;
    private float _lastPositionOnGroundY;

    private void Start()
    {
        _body = GetComponent<Rigidbody2D>();
        _player = GetComponent<PlayerController>();
        Health = MaxHealth;

        if (Slider == null)
            Slider = GameObject.Find("HealthBar").GetComponent<Slider>();

        UpdateLastGroundPosition();
    }

    private void Update()
    {
        if (_player.IsOnGround)
        {
            var fallDistance = _lastPositionOnGroundY - transform.position.y;
            if (!_player.IsInWater && fallDistance > FallDamageHeight)
            {
                Health -= Mathf.Ceil(fallDistance / FallDamageHeight);
                AudioManager.Instance.Play(AudioClipStore.Instance.FallClips);
            }

            // has touched the ground
            if (!_isOnGroundPrev)
            {
                // don't print every fall; it's annoying
                if (fallDistance > FallDamageHeight)
                    print(_player.IsInWater ? "Fell into water." : "Fell onto ground. distance = " + fallDistance);
            }

            UpdateLastGroundPosition();
        }

        _isOnGroundPrev = _player.IsOnGround;

        UpdateUI(Health / MaxHealth);
    }

    private void UpdateLastGroundPosition()
    {
        _lastPositionOnGroundY = transform.position.y;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "DeathZone")
        {
            Health = 0;
        }
    }

    private void UpdateUI(float value)
    {
        Slider.value = value;
    }
}

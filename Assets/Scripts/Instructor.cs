using UnityEngine;
using UnityEngine.SceneManagement;

public class Instructor : MonoBehaviour
{
    public static Instructor Instance;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        if (SceneManager.GetActiveScene().name.EndsWith("1"))
        {
            TextController.Instance.SetText("Use WASD to move. Press R to reset level.", 2);
        }
    }

    public void DisplayFireMessage()
    {
        if (SceneManager.GetActiveScene().Level() == 1)
            TextController.Instance.SetText("Use left click to control the exothermic orb.", 3);
    }

    public void DisplayIceMessage()
    {
        if (SceneManager.GetActiveScene().Level() == 3)
            TextController.Instance.SetText("Use right click to control the exothermic orb.", 3);
    }
}

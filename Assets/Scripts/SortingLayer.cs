﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingLayer : MonoBehaviour
{
    public string layerName;

	// Use this for initialization
	void Start ()
	{
	    GetComponent<MeshRenderer>().sortingLayerName = layerName;
	}
	
}

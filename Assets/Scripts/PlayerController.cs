﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TwoDimensionalMovement))]
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{

    public Animator PlayerAnimator;

    public Transform GroundCheck;
    public LayerMask GroundMask;

    public float VerticalInputTime = .5f;

    private SpriteRenderer _spriteRenderer;
    private TwoDimensionalMovement _movement;
    private float _verticalInputCounter;
    public bool IsOnGround;

    private float _horizontalInput;
    private float _verticalInput;
    private bool _verticalUp;

    private float _originalGravityScale;
    private float _originalTargetSpeed;
    private Rigidbody2D _rigidbody;
    public bool IsInWater;
    public LayerMask WaterMask;
    // Use this for initialization
    void Awake()
    {
        _movement = GetComponent<TwoDimensionalMovement>();
        _verticalInputCounter = VerticalInputTime;
        _rigidbody = GetComponent<Rigidbody2D>();
        _originalGravityScale = _rigidbody.gravityScale;
        _originalTargetSpeed = _movement.TargetSpeed;
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    void Update()
    {
        _horizontalInput = Input.GetAxisRaw("Horizontal");
        _verticalInput = Input.GetAxisRaw("Vertical");
        _verticalUp = Input.GetButtonUp("Vertical");
        PlayerAnimator.speed = Mathf.Abs(_rigidbody.velocity.x) / _movement.TargetSpeed;
        PlayerAnimator.SetFloat("Speed", PlayerAnimator.speed);
        if (_verticalUp)
            _verticalInputCounter = 0;
        if (Math.Pow(_verticalInputCounter, 2) <= float.Epsilon && IsOnGround)
        {
            _verticalInputCounter = VerticalInputTime;
        }
        CheckDeath();
    }

    private void CheckDeath()
    {
        var isDead = GetComponent<PlayerHealth>().Health <= 0;
        if (isDead)
        {
            StartCoroutine(DeathRoutine());
        }
    }

    private IEnumerator DeathRoutine()
    {
        var levelManager = GameObject.FindObjectOfType<LevelManager>();
        levelManager.RestartLevel();
        yield break;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.localScale = new Vector3(Math.Pow(_horizontalInput,2) > 0 ?_horizontalInput : transform.localScale.x, transform.localScale.y, transform.localScale.z);
        _movement.AccelaretedHorizontalMovement(_horizontalInput);


        IsInWater = CheckWater();

        if (_verticalInput > 0 && _verticalInputCounter > float.Epsilon)
        {
            var time = IsInWater ? Time.deltaTime / 2 : Time.deltaTime;
            _verticalInputCounter -= time;
            _movement.Jump(_verticalInputCounter);
        }
        if(_verticalInput < 0 && IsInWater)
        {
            _movement.AccelaretedVerticalMovement(_verticalInput);
        }
        if (IsInWater)
            SetLowGravity();
        else
            ResetGravity();
    }

    private bool CheckWater()
    {
        var size = _spriteRenderer.sprite.rect.size;
        var hit = Physics2D.Linecast(transform.position + new Vector3(0, size.y / 2), transform.position - new Vector3(0, size.y * 1.5f), WaterMask);
        return hit.transform != null;
    }

    public void SetLowGravity()
    {
        _rigidbody.gravityScale = _originalGravityScale * .2f;
        _movement.TargetSpeed = _originalTargetSpeed * .5f;
        _rigidbody.drag = 2;
    }
    public void ResetGravity()
    {
        _rigidbody.gravityScale = _originalGravityScale;
        _movement.TargetSpeed = _originalTargetSpeed;
        _rigidbody.drag = 0;

    }
}

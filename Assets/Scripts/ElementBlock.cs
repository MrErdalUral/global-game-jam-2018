using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ElementBlock : MonoBehaviour
{
    public Element CurrentElement;

    public bool IsTouchingFireOrb;
    public bool IsTouchingIceOrb;

    public float TileChangeTime;

    private Vector3 _collisionPosition;
    private CompositeCollider2D _collider;
    private Tilemap _tilemap;

    private WaitForSeconds _freezeTime;

    private Dictionary<Element, LayerTileTriggerData> _elementData;

    public bool CanExpand
    {
        get { return CurrentElement == Element.Metal; }
    }

    public bool CanBurn
    {
        get { return CurrentElement == Element.Wood; }
    }

    private void Awake()
    {
        var tiles = Resources.FindObjectsOfTypeAll<Tile>();
        _freezeTime = new WaitForSeconds(TileChangeTime);

        _elementData = new Dictionary<Element, LayerTileTriggerData>
        {
            {Element.Water, new LayerTileTriggerData("Water", tiles.Single(t => t.name == "Water"), true)},
            {Element.Ice, new LayerTileTriggerData("Ice", tiles.Single(t => t.name == "Ice"), false)},
        };

        _collider = GetComponent<CompositeCollider2D>();
    }

    private void Start()
    {
        _tilemap = GetComponent<Tilemap>();
    }

    private void Update()
    {
        // do nothing if both or none are touching
        if (IsTouchingFireOrb == IsTouchingIceOrb)
            return;

        FireOrbAffect();
        IceOrbAffect();
        UpdateLayer();
    }

    public void FireOrbAffect()
    {
        if (!IsTouchingFireOrb)
            return;

        if (CurrentElement == Element.Water)
            return;

        var newElement = ElementStateMachine.TouchFireOrb(CurrentElement);

        var oldTile = _elementData[CurrentElement].Tile;
        var newTile = _elementData[newElement].Tile;

        var positions = FindTilesToConvert(_collisionPosition, oldTile);
        StartCoroutine(SetTilesCoroutine(positions, newTile));

        CurrentElement = newElement;
    }

    public void IceOrbAffect()
    {
        if (!IsTouchingIceOrb)
            return;

        if (CurrentElement == Element.Ice)
            return;

        var newElement = ElementStateMachine.TouchIceOrb(CurrentElement);

        var oldTile = _elementData[CurrentElement].Tile;
        var newTile = _elementData[newElement].Tile;

        var positions = FindTilesToConvert(_collisionPosition, oldTile);
        StartCoroutine(SetTilesCoroutine(positions, newTile));

        CurrentElement = newElement;
    }

    private void UpdateLayer()
    {
        LayerTileTriggerData layerTileTriggerData;
        if (_elementData.TryGetValue(CurrentElement, out layerTileTriggerData))
        {
            gameObject.layer = layerTileTriggerData.Layer;
            _collider.isTrigger = layerTileTriggerData.IsTrigger;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var touchedFire = other.CompareTag("Fire Orb");
        var touchedIce = other.CompareTag("Ice Orb");

        if (!touchedFire && !touchedIce)
            return;

        _collisionPosition = other.transform.position;

        if (touchedFire)
            IsTouchingFireOrb = true;

        if (touchedIce)
            IsTouchingIceOrb = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var touchedFire = other.CompareTag("Fire Orb");
        var touchedIce = other.CompareTag("Ice Orb");

        if (touchedFire)
            IsTouchingFireOrb = false;

        if (touchedIce)
            IsTouchingIceOrb = false;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        var touchedFire = other.gameObject.CompareTag("Fire Orb");
        var touchedIce = other.gameObject.CompareTag("Ice Orb");

        if (!touchedFire && !touchedIce)
            return;

        _collisionPosition = other.transform.position;

        if (touchedFire)
            IsTouchingFireOrb = true;

        if (touchedIce)
            IsTouchingIceOrb = true;
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        var touchedFire = other.gameObject.CompareTag("Fire Orb");
        var touchedIce = other.gameObject.CompareTag("Ice Orb");

        if (touchedFire)
            IsTouchingFireOrb = false;

        if (touchedIce)
            IsTouchingIceOrb = false;
    }

    private IEnumerable<Vector3Int> FindTilesToConvert(Vector3 v, Tile from)
    {
        var cellOrigin = _tilemap.WorldToCell(v);
        bool isStartingPositionFound = false;
        Vector3Int startingPosition = default(Vector3Int);

        for (var i = -1; i < 2; i++)
        {
            for (var j = -1; j < 2; j++)
            {
                var cellPosition = cellOrigin + new Vector3Int(j, i, 0);
                var tile = _tilemap.GetTile(cellPosition);
                if (tile != null && tile.name == from.name)
                {
                    startingPosition = cellPosition;
                    isStartingPositionFound = true;
                }
            }
        }

        var positions = new HashSet<Vector3Int>();

        if (!isStartingPositionFound)
        {
//            print("Starting position not found.");
            return positions;
        }

//        TraverseDepthFirst(startingPosition, from, positions);
        TraverseBreadthFirst(startingPosition, from, positions);

        return positions;
    }

    private void TraverseDepthFirst(Vector3Int current, Tile tile, HashSet<Vector3Int> visited)
    {
        var currentTile = _tilemap.GetTile(current);
        if (visited.Contains(current) || currentTile == null || currentTile.name != tile.name)
            return;

        visited.Add(current);

        TraverseDepthFirst(current + Vector3Int.right, tile, visited);
        TraverseDepthFirst(current + Vector3Int.down, tile, visited);
        TraverseDepthFirst(current + Vector3Int.left, tile, visited);
        TraverseDepthFirst(current + Vector3Int.up, tile, visited);
    }

    private void TraverseBreadthFirst(Vector3Int current, Tile matchingTile, HashSet<Vector3Int> visited)
    {
        var open = new Queue<Vector3Int>();
        var neighbors = new Vector3Int[4];

        open.Enqueue(current);

        while (open.Count > 0)
        {
            var parent = open.Dequeue();

            neighbors[0] = parent + Vector3Int.right;
            neighbors[1] = parent + Vector3Int.down;
            neighbors[2] = parent + Vector3Int.left;
            neighbors[3] = parent + Vector3Int.up;

            for (var i = 0; i < 4; i++)
            {
                var child = neighbors[i];

                var childTile = _tilemap.GetTile(child);
                if (visited.Contains(child) || childTile == null || matchingTile.name != childTile.name)
                    continue;

                if (!open.Contains(child))
                    open.Enqueue(child);
            }

            visited.Add(parent);
        }
    }

    private IEnumerator SetTilesCoroutine(IEnumerable<Vector3Int> positions, Tile newTile)
    {
        foreach (var position in positions)
        {
            yield return _freezeTime;
            _tilemap.SetTile(position, newTile);

            AudioManager.Instance.Play(AudioClipStore.Instance.FreezeClips, maxPitchOffset: .15f);
        }
    }

    public struct LayerTileTriggerData
    {
        public readonly int Layer;
        public readonly Tile Tile;
        public readonly bool IsTrigger;

        public LayerTileTriggerData(int layer, Tile tile, bool isTrigger)
        {
            Layer = layer;
            Tile = tile;
            IsTrigger = isTrigger;
        }

        public LayerTileTriggerData(string layerName, Tile tile, bool isTrigger) : this(
            LayerMask.NameToLayer(layerName), tile, isTrigger)
        {
        }
    }
}

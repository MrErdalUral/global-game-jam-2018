using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    private string _textContent;

    public float DefaultDuration = 2;
    public float PanelFillStep = .1f;
    public Text Text;
    public Image Panel;

    public static TextController Instance;

    private void Awake()
    {
        if (Instance != null)
        {
            Instance.Init();
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        SceneManager.activeSceneChanged += delegate { Init(); };
        Init();
    }

    private void Init()
    {
        if (Panel == null)
            Panel = GameObject.Find("InfoTextPanel").GetComponent<Image>();

        if (Text == null)
            Text = Panel.GetComponentInChildren<Text>();
    }

    private void Update()
    {
        if (Text != null && _textContent != Text.text)
        {
            Text.text = _textContent;
        }
    }

    private IEnumerator ResetCoroutine(float countdown)
    {
        yield return new WaitForSeconds(countdown);

        _textContent = "";

        StartCoroutine(HidePanelCoroutine());
    }

    public void SetText(string textContent)
    {
        SetText(textContent, DefaultDuration);
    }

    public void SetText(string textContent, float duration)
    {
        StartCoroutine(ShowPanelCoroutine(textContent));
        StartCoroutine(ResetCoroutine(duration));
    }

    private IEnumerator HidePanelCoroutine()
    {
        while (Panel.fillAmount > 0)
        {
            Panel.fillAmount -= PanelFillStep;
            yield return Wait.ForEndOfFrame;
        }
    }

    private IEnumerator ShowPanelCoroutine(string textContent)
    {
        while (Panel.fillAmount < 1)
        {
            Panel.fillAmount += PanelFillStep;
            yield return Wait.ForEndOfFrame;
        }

        _textContent = textContent;
    }
}

using UnityEngine;
using System.Collections;

public class ExpandableBlock : MonoBehaviour
{
    public SpriteRenderer Tint;
    public Color HotColor;
    public Color ColdColor;
    private Color _originalColor;
    public Vector3 ExpandedScale = Vector3.one;

    public Vector3 ShrinkedScale = Vector3.one;

    public bool IsIncontactWithFireOrb;
    public bool IsIncontactWithIceOrb;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _originalColor = Tint.color;
    }

    private void Start()
    {
    }

    private void Update()
    {

        if (IsIncontactWithFireOrb == IsIncontactWithIceOrb)
            return;
        if (IsIncontactWithFireOrb && !IsIncontactWithIceOrb)
        {
            Tint.color = Color.Lerp(Tint.color, HotColor, Time.deltaTime);
            Expand();
        }
        else if (IsIncontactWithIceOrb && !IsIncontactWithFireOrb)
        {
            Tint.color = Color.Lerp(Tint.color, ColdColor, Time.deltaTime);
            Contract();
        }
    }

    public void Expand()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, ExpandedScale, Time.deltaTime);
    }

    public void Contract()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, ShrinkedScale, Time.deltaTime);
    }

    public void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Fire Orb")
            IsIncontactWithFireOrb = true;
        else if (collision.gameObject.tag == "Ice Orb")
            IsIncontactWithIceOrb = true;
    }
    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Fire Orb")
            IsIncontactWithFireOrb = false;
        else if (collision.gameObject.tag == "Ice Orb")
            IsIncontactWithIceOrb = false;
    }
}

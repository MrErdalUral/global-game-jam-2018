﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbThrower : MonoBehaviour
{
    public float Speed = 100;
    public Rigidbody2D FireProjectile;
    public Rigidbody2D IceProjectile;

    public bool IsFireOrbReady;
    public bool IsIceOrbReady;
    private Rigidbody2D _fireInstance;
    private Rigidbody2D _iceInstance;
    // Use this for initialization
    void Awake()
    {
        IsFireOrbReady = false;
        IsIceOrbReady = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && IsFireOrbReady)
        {
            var target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            target.z = 0;
            var direction = (target - transform.position).normalized;
            _fireInstance = Instantiate(FireProjectile, transform.position + direction * 7, Quaternion.identity);
            _fireInstance.velocity = direction * Speed;
            IsFireOrbReady = false;

            AudioManager.Instance.Play(AudioClipStore.Instance.ThrowBallClips, maxPitchOffset: .2f);
        }
        else if (!Input.GetMouseButton(0) && !IsFireOrbReady && _fireInstance)
        {
            var direction = (transform.position - _fireInstance.transform.position).normalized;
            _fireInstance.velocity = direction * Speed;
            _fireInstance.GetComponent<Orb>().IsStuck = false;
        }

        if (Input.GetMouseButtonDown(1) && IsIceOrbReady)
        {
            var target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            target.z = 0;
            var direction = (target - transform.position).normalized;
            _iceInstance = Instantiate(IceProjectile, transform.position + direction * 7, Quaternion.identity);
            _iceInstance.velocity = direction * Speed;
            IsIceOrbReady = false;
            AudioManager.Instance.Play(AudioClipStore.Instance.ThrowBallClips, maxPitchOffset: .2f);
        }
        else if (!Input.GetMouseButton(1) && !IsIceOrbReady && _iceInstance)
        {
            var direction = (transform.position - _iceInstance.transform.position).normalized;
            _iceInstance.velocity = direction * Speed;
            _iceInstance.GetComponent<Orb>().IsStuck = false;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Fire Orb"))
        {
            Destroy(collision.gameObject);
            IsFireOrbReady = true;
        }
        else if (collision.gameObject.CompareTag("Ice Orb"))
        {
            Destroy(collision.gameObject);
            IsIceOrbReady = true;

        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Fire Orb"))
        {
            Destroy(collision.gameObject);
            IsFireOrbReady = true;
            Instructor.Instance.DisplayFireMessage();
        }
        else if (collision.gameObject.CompareTag("Ice Orb"))
        {
            Destroy(collision.gameObject);
            IsIceOrbReady = true;
            Instructor.Instance.DisplayIceMessage();
        }
    }
}

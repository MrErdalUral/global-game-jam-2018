﻿using UnityEngine;

public static class Wait
{
    public static readonly WaitForSeconds ForSeconds1 = new WaitForSeconds(1);
    public static readonly WaitForSeconds ForSeconds2 = new WaitForSeconds(2);
    public static readonly WaitForSeconds ForSeconds3 = new WaitForSeconds(3);
    public static readonly WaitForSeconds ForSeconds4 = new WaitForSeconds(4);
    public static readonly WaitForSeconds ForSeconds5 = new WaitForSeconds(5);

    public static readonly WaitForSeconds[] ForSeconds1_5 = new WaitForSeconds[]
        {ForSeconds1, ForSeconds2, ForSeconds3, ForSeconds4, ForSeconds5};

    public static readonly WaitForEndOfFrame ForEndOfFrame = new WaitForEndOfFrame();
}

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioClip DefaultClip;
    public int Instances;

    public float MaxDistance;

	public int PlayingInstances;

    [Tooltip("Used for determining how many instances are required.")]
	public int MaxPlayingInstances;

    private AudioSource[] _audioSources;
    private bool[] _availability; // if (_availability[i]) "available"

    [SerializeField]
    private AudioListener _audioListener;

    public AudioSource AvailableSource
    {
        get
        {
            for (var i = Instances - 1; i >= 0; i--)
            {
                if (_availability[i])
                {
                    _availability[i] = false;
					var audioSource = _audioSources[i];
                    return audioSource;
                }
            }

            return null;
            // throw new Exception("No audio sources available.");
        }
    }

    private AudioSource InitializeAudioSource()
    {
        var go = new GameObject("AudioSource");
        go.transform.SetParent(transform);

        var audioSource = go.AddComponent<AudioSource>();
        audioSource.maxDistance = MaxDistance;
        audioSource.spatialBlend = .75f;
        audioSource.dopplerLevel = 0;

        return audioSource;
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Instance.Init();
            Destroy(gameObject);
            return;
        }

        SceneManager.activeSceneChanged += delegate { Init(); };
        Instance = this;
        DontDestroyOnLoad(gameObject);

        _availability = new bool[Instances];

        _audioSources = new AudioSource[Instances];
        for (var i = Instances - 1; i >= 0; i--)
        {
            var audioSource = InitializeAudioSource();
            audioSource.clip = DefaultClip;
            _audioSources[i] = audioSource;
            _availability[i] = true;
        }

        Init();
    }

    private void Init()
    {
//        if (_audioListener == null)
            _audioListener = FindObjectOfType<AudioListener>();
    }

    public AudioSource Play(AudioClip clip, float volume = 1.0f, bool loop = false, float maxPitchOffset = 0,
        float delay = 0, Transform at = null)
    {
        var audioSource = AvailableSource;
        if (audioSource == null)
            return null;

        if (at != null)
            audioSource.transform.position = at.position;
        else
            audioSource.transform.position = _audioListener.transform.position;

        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.loop = loop;
        audioSource.RandomizePitch(maxPitchOffset);

        audioSource.Play(clip.Delay(delay));
        PlayingInstances++;
	    if (PlayingInstances > MaxPlayingInstances)
		    MaxPlayingInstances = PlayingInstances;

        if (!audioSource.loop)
            Release(audioSource);

        return audioSource;
    }

    public AudioSource Play(AudioClip[] clips, float volume = 1.0f, bool loop = false, float maxPitchOffset = 0,
        float delay = 0, Transform at = null)
    {
        return Play(clips.RandomElement(), volume, loop, maxPitchOffset, delay, at);
    }

    public void Release(AudioSource audioSource)
    {
        StartCoroutine(FreeAudioSource(audioSource));
    }

    private IEnumerator FreeAudioSource(AudioSource audioSource)
    {
        while (audioSource.isPlaying)
        {
            yield return Wait.ForEndOfFrame;
        }

        for (var i = _audioSources.Length - 1; i >= 0; i--)
        {
            if (_audioSources[i] == audioSource)
            {
                _availability[i] = true;
            }
        }

		PlayingInstances--;
    }

    public IEnumerator RandomizeLoopPitchCoroutine(AudioSource audioSource, float maxPitchOffset = 0)
    {
        while (true)
        {
            yield return Wait.ForSeconds1_5.RandomElement();
            var newPitch = 1 + UnityEngine.Random.value * maxPitchOffset - maxPitchOffset;

            if (audioSource.pitch < newPitch)
            {
                while (audioSource.pitch < newPitch)
                {
                    audioSource.pitch += .005f;
                    yield return Wait.ForEndOfFrame;
                }
            }
            else if (audioSource.pitch > newPitch)
            {
                while (audioSource.pitch > newPitch)
                {
                    audioSource.pitch -= .005f;
                    yield return Wait.ForEndOfFrame;
                }
            }
        }
    }

    public void StopAll()
    {
        for (var i = _audioSources.Length - 1; i >= 0; i--)
        {
            var audioSource = _audioSources[i];
            audioSource.Stop();
            Release(audioSource);
        }
    }
}

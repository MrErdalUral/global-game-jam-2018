﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{

    public Rigidbody2D ProjectilePrefab;
    public float CooldownTime;
    public Vector2 SpawnVelocity;
    public float ProjectileDestroyTime;
    public float WaitTime = 0;
    private float _counter;



    // Update is called once per frame
    void Update()
    {
        if(WaitTime > 0)
        {
            WaitTime -= Time.deltaTime;
            return;
        }
        if (ProjectilePrefab == null) return;
        _counter -= Time.deltaTime;
        if (_counter < 0)
        {
            var instance = Instantiate(ProjectilePrefab, transform.position, Quaternion.identity);
            instance.velocity = SpawnVelocity;
            StartCoroutine(DestroyRoutine(instance));
            _counter = CooldownTime;
        }
    }

    private IEnumerator DestroyRoutine(Rigidbody2D instance)
    {
        yield return new WaitForSeconds(ProjectileDestroyTime);
        instance.gameObject.SetActive(false);
        Destroy(instance.gameObject);

    }
}

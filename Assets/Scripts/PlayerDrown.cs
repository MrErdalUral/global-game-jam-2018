using UnityEngine;
using UnityEngine.UI;

public class PlayerDrown : MonoBehaviour
{
    public float BreathDuration = 1;
    public float UnderwaterDuration;
    public float DamagePerSecond;

    public Slider Slider;

    private SpriteRenderer _spriteRenderer;
    private PlayerController _player;
    private PlayerHealth _playerHealth;

    private readonly RaycastHit2D[] _hits = new RaycastHit2D[1];
    private bool _shouldPlayDrownClip;

    private void Awake()
    {
        _player = GetComponent<PlayerController>();
        _playerHealth = GetComponent<PlayerHealth>();
        _spriteRenderer = GetComponent<SpriteRenderer>();

        if (Slider == null)
            Slider = GameObject.Find("BreathBar").GetComponent<Slider>();

        _shouldPlayDrownClip = true;
    }

    private void Update()
    {
        if (!_player.IsInWater)
        {
            FillBreath();
        }
        else
        {
            var size = _spriteRenderer.sprite.rect.size;
            var targetPosition = transform.position + new Vector3(0, size.y);
            var isUnderwater =
                Physics2D.LinecastNonAlloc(transform.position, targetPosition, _hits, _player.WaterMask) > 0;

            if (isUnderwater)
            {
                UnderwaterDuration += Time.deltaTime;
            }
            else
            {
                FillBreath();
            }

            if (UnderwaterDuration > BreathDuration)
            {
                if (_shouldPlayDrownClip)
                {
                    AudioManager.Instance.Play(AudioClipStore.Instance.DrownClips);
                    _shouldPlayDrownClip = false;
                }
                _playerHealth.Health -= DamagePerSecond * Time.deltaTime;
            }
        }

        UpdateUI(1 - UnderwaterDuration / BreathDuration);
    }

    private void FillBreath()
    {
        UnderwaterDuration = Mathf.Clamp(UnderwaterDuration - Time.deltaTime * 8, 0, BreathDuration);
        _shouldPlayDrownClip = true;
    }

    private void UpdateUI(float value)
    {
        Slider.value = value;
    }
}
